public class MyDate {
    int year, month, day;
    int dayOfMonths[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; 

    public MyDate(int day, int month, int year) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public MyDate(MyDate d) {
        this(d.day, d.month, d.year);
    }

    public void incrementDay() {
        incrementDay(1);
    }

    
    public void incrementDay(int x) {
        int d = this.day + x;
        if (this.year % 4 != 0) dayOfMonths[1] = 28;
        if (d > dayOfMonths[this.month-1]) {
            if (this.month==12) {this.month = (d / 30+1)%12; this.year+=(d/30+1)/12+1; this.day = (this.day + x) % 30;}
            else{this.day = this.day + x - dayOfMonths[this.month-1]; this.month += (d / 30+1)%12; this.month %= 12; this.year += (d/30+1)/12;}
        } else this.day = d;
        dayOfMonths[1] = 29;
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public void incrementYear(int x) {
        if ((this.year+x) % 4 != 0) {
            if (this.day == 29 && this.month == 2) {this.day = 28; this.year += x;}
            else this.year += x;
        } else this.year += x;
    }
    
    public void decrementDay() {
        decrementDay(1);
    }
        
    public void decrementDay(int x) {
         if (x == 1) { 
            if (this.year % 4 == 0) {
                this.month--;
                if (this.month == 2) this.day = 29;
                else this.day = dayOfMonths[this.month-1];
            }
         } else if (x == 30){
            this.month -= this.day / 30;
            this.day = dayOfMonths[this.month-1];
         } else {this.day -= x;}
    }

    public void decrementYear() {
        decrementYear(1);
    }

    public void decrementYear(int x) {
        this.year -= x;
        if (this.year % 4 != 0 && this.month == 2 && this.day > 28) {this.day = 28;}
    }

    public void decrementMonth() {
        decrementMonth(1);
    }
    
    public void decrementMonth(int x) {
        // 1 - 1 = 0 aralık -> 1 - 2 = -1 kasım -> 1 - 3 = -2 ekim
        if (this.month - x <= 0) {
            this.month = 12 + this.month - x;
            this.year -= 1+x/12;
            if (this.day > dayOfMonths[this.month-1]) this.day = dayOfMonths[this.month-1];
        } else {
            this.month -= x;
            if (this.day > dayOfMonths[this.month-1]) this.day = dayOfMonths[this.month-1];
        }
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementMonth(int x) {
        this.year += (this.month+x)/12;
        x = x % 12;
        this.month = (this.month+x)%12;
        if (this.day > dayOfMonths[this.month-1]) this.day = dayOfMonths[this.month-1];
    }

    public boolean isBefore(MyDate d) {
        if (d.year > this.year) return true;
        else if (d.year < this.year) return false;
        else if (d.month > this.month) return true;
        else if (d.month < this.month) return false;
        else if (d.day > this.day) return true;
        else if (d.day < this.day) return false;
        else return false;
    }
        
    public boolean isAfter(MyDate d) {
        if (this.day == d.day && this.year == d.year && this.month == d.month) return false;
        else if (isBefore(d) == true) return false;
        return true;
    }

    public int dayDifference(MyDate d) {
        MyDate n = new MyDate(d.day, d.month, d.year);
        int i = 0;
        while (true) {
            n.incrementDay();
            i++;
            if (n.day == this.day && n.month == this.month && n.year == this.year) break;
        }
        return i;
    }

    @Override
    public String toString() {
        return this.year + "-" + this.month + "-" + this.day;
    }

}
